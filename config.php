<?php
include('Smarty/libs/Smarty.class.php');
date_default_timezone_set("Europe/Berlin");

$smarty = new Smarty;
$smarty->clearCompiledTemplate();
$smarty->force_compile = true;
$smarty->caching = false;
$smarty->setCompileDir('/tmp/template_c');


// SMARTY DATA INITIALIZATION //

$jsonContent = file_get_contents("test.data.json");
$array = json_decode($jsonContent, true);
foreach ($array as $key => $value)
{
    $smarty->assign($key, $value);
}

$smarty->assign('layout', 'test.layout.tpl');


// SMARTY DUMMY CONFIGURATION //

smartyRegisterFunction($smarty, 'function', 'l', 'smartyTranslate');
smartyRegisterFunction($smarty, 'function', 'url', 'getUrlSmarty');
smartyRegisterFunction($smarty, 'function', 'hook', 'smartyHook');
smartyRegisterFunction($smarty, 'modifier', 'classnames', 'smartyClassnames');
smartyRegisterFunction($smarty, 'function', 'convertPrice', 'convertPrice');
smartyRegisterFunction($smarty, 'function', 'convertPriceWithCurrency', 'convertPriceWithCurrency');

function smartyTranslate($params)
{
    /*if (!isset($params['sprintf'])) {
        $params['sprintf'] = array();
    }*/

     $msg = $params['s']; //str_replace('\'', '\\\'', $params['s']);

    if ($params['sprintf'] !== null) {
        $msg = checkAndReplaceArgs($msg, $params['sprintf']);
    }

    return $msg;
}

function checkAndReplaceArgs($string, $args)
{
    if(is_string($args)) {
        $args = array("%__value__%" => $args);
    }


    if (is_array($args) && !empty($args)) {
        //return vsprintf("Order Reference %%reference ", Array("reference" => "aagggg", "%reference%" => "bb"));

        if(strpos( $string, array_keys($args)[0] ) !== false) {
             foreach ($args as $key => $value)
            {
                $string = str_replace($key , $value, $string);

               // $smarty->assign($key, $value);
            }
        } else {
            return vsprintf($string, $args);
        }


        // return vsprintf($string, $args);

        //if( strpos( $container, $find ) !== false) {
    }

    return $string;
}


function smartyHook($params)
{
    return "";
}

function getUrlSmarty($params, &$smarty)
{
    return "#";
}

function smartyClassnames(array $classnames)
{
    return "";
}


function convertPrice($params, &$smarty)
{
    return $params['price'];
}

function convertPriceWithCurrency($params, &$smarty)
{
    return $params['price']." ".$params['currency'];
}

function smartyRegisterFunction($smarty, $type, $function, $params, $lazy = false)
{
    if (!in_array($type, array('function', 'modifier', 'block'))) {
        return false;
    }

    // lazy is better if the function is not called on every page
    if ($lazy) {
        $lazy_register = SmartyLazyRegister::getInstance();
        $lazy_register->register($params);

        if (is_array($params)) {
            $params = $params[1];
        }

        // SmartyLazyRegister allows to only load external class when they are needed
        $smarty->registerPlugin($type, $function, array($lazy_register, $params));
    } else {
        $smarty->registerPlugin($type, $function, $params);
    }
}


?>

